import React from "react";
import logo from "./logo.svg";
import "./App.css";

function MyButton() {
  function handleClick(e: any) {
    e.preventDefault();
    console.log("The link was clicked.");
  }

  return (
    <div className="App">
      {/* <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header> */}
      <button onClick={handleClick}>押してみてね</button>
    </div>
  );
}

export default MyButton;
