import React from "react";
import { render } from "@testing-library/react";
import MyButton from "../MyButton";

test("renders learn react link", () => {
  const { getByText } = render(<MyButton />);
  const linkElement = getByText(/押してみてね/i);
  expect(linkElement).toBeInTheDocument();
});
